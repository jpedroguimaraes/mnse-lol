// TODO: fazer um play pause and mute - https://github.com/goldfire/howler.js/
var background_music = new Howl({
  urls: ['./sounds/main/djsona.mp3'],
  loop: true,
  volume: 0.5,
});

var welcome = new Howl({
    urls: ['./sounds/main/welcome.mp3'],
    autoplay: true,
    onload: function() {
        background_music.play();
    }
});
function play_music()
{
    background_music.play();
}
function pause_music()
{
    background_music.pause();
}
function toggleRow(element)
{
 var parent = element.parentNode;
    var node = parent.querySelector(".extend");
    var arrow = parent.querySelector(".glyphicon");
    if(node.style.display == "none" || node.style.display =="")
    {
        node.style.display = "block";
        arrow.className = "glyphicon glyphicon-menu-down"

    }
    else
    {
        node.style.display = "none";
        arrow.className = "glyphicon glyphicon-menu-right"
    }
}
function openHash()
{
    console.log("has changed");
    hash = location.hash;
    if(hash)
    {
        var node = document.querySelector(hash);
        if(node)
        {
            var nodeToExtend = node.querySelector(".extend");
            var arrow = node.querySelector(".glyphicon");
            if(nodeToExtend.style.display == "none" || nodeToExtend.style.display =="")
            {
                nodeToExtend.style.display = "block";
                arrow.className = "glyphicon glyphicon-menu-down"

            }
        }
    }
}