function toggle_video()
{
    var video = document.getElementById("cinematic_video");
    if(video.paused)
        video.play();
    else
        video.pause();
}

var sound = new Howl({
    urls: ['./sounds/launcher/reconected.mp3'],
    onend: function() {
        window.location.href = "./main.html";
    }
});
function launcher_clicked()
{
    var video = document.getElementById("cinematic_video");
    video.pause();
    sound.play();
};
function init()
{
    var video = document.getElementById("cinematic_video")
    video.addEventListener("click",toggle_video);
    video.play();

}
/*
//alternative
var audio = new Audio('sounds/launcher/welcome.ogg');
audio.play();
*/