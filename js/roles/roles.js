// TODO: fazer um play pause and mute - https://github.com/goldfire/howler.js/
var background_music = new Howl({
  urls: ['./sounds/about/party.mp3'],
  loop: true,
  volume: 0.5,
});

var welcome = new Howl({
    urls: ['./sounds/about/quest.mp3'],
    autoplay: true,
    onload: function() {
        background_music.play();
    }
});
function play_music()
{
    background_music.play();
}
function pause_music()
{
    background_music.pause();
}
function toggleMap(opt)
{
  var map = document.getElementById("map");
  var desc = document.getElementById("description");
  if(opt == 'top') {
    map.src = './images/roles/top/map_roles.html';
    desc.innerHTML = "<p>A role usually taken by tank champions.</p>";
  } else if(opt == 'mid') {
    map.src = './images/roles/mid/map_roles.html';
    desc.innerHTML = "In this role, it's very useful to select champions that deal a lot of damage, both via attacks or abilities.";
  } else if(opt == 'jun') {
    map.src = './images/roles/jun/map_roles.html';
    desc.innerHTML = "The champions in this role farm the mobs in the jungle, usually helping the lanes with ganks.";
  } else if(opt == 'adc') {
    map.src = './images/roles/bot/map_roles.html';
    desc.innerHTML = "A role occupied by champions with great attack damage and attack speed.";
  } else if(opt == 'sup') {
    map.src = './images/roles/bot/map_roles.html';
    desc.innerHTML = "Champions in this role have the task to keep the AD Carry alive in the lane phase and help the rest of the team with wards, healing and boosts.";
  }
}
window.onload = function()
{
  var top = document.getElementById("top");
  top.onclick = function() {
    toggleMap('top');
    return true;
  }
  var mid = document.getElementById("mid");
  mid.onclick = function() {
    toggleMap('mid');
    return true;
  }
  var jun = document.getElementById("jun");
  jun.onclick = function() {
    toggleMap('jun');
    return true;
  }
  var adc = document.getElementById("adc");
  adc.onclick = function() {
    toggleMap('adc');
    return true;
  }
  var sup = document.getElementById("sup");
  sup.onclick = function() {
    toggleMap('sup');
    return true;
  }
}