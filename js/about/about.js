// TODO: fazer um play pause and mute - https://github.com/goldfire/howler.js/
var background_music = new Howl({
  urls: ['./sounds/about/party.mp3'],
  loop: true,
  volume: 0.5,
});

var welcome = new Howl({
    urls: ['./sounds/about/quest.mp3'],
    autoplay: true,
    onload: function() {
        background_music.play();
    }
});
function play_music()
{
    background_music.play();
}
function pause_music()
{
    background_music.pause();
}
